(function () {
  'use strict';

  function save (data, cb) {
    fetch('/api', {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }).then(function (response) {
        if (response.status >= 200 && response.status < 300) {
          response.text();
          showSnackbar('Saved.');
        } else {
          showSnackbar('Something went wrong!');
        }
        cb();
      });
  }


  function showSnackbar (message, duration) {
    var snackbarContainer = document.querySelector('#wiki-snackbar'),
      data = {
        message: message,
        timeout: duration || 2000
      };

    snackbarContainer.MaterialSnackbar.showSnackbar(data);
  }

  window.onload = function () {
    var edit = document.getElementById('edit'),
      cancel = document.getElementById('cancel'),
      article = document.getElementById('article'),
      setButtonIcon = function (element, icon) {
        element.children[0].innerHTML = icon;
      },
      close = function () {
        // hide cancel button and clear status area
        article.contentEditable = false;
        setButtonIcon(edit, 'mode_edit');
        cancel.hidden = true;
      },
      getValue = function (id) {
        // return value from an id
        return document.getElementById(id).value;
      };

    // register for the click event on edit button
    edit.addEventListener('click', function () {
      // if contentEditable is true, then user is already in edit mode
      if (article.contentEditable === "true") {
        showSnackbar('Saving...');
        save({
          id: getValue('article_id'),
          title: document.getElementById('title').innerHTML,
          name: getValue('article_name'),
          data: article.innerHTML
        }, close);
      } else {
        article.contentEditable = true;
        article.focus();
        setButtonIcon(edit, 'save');
        cancel.hidden = false;
      }
    });

    cancel.addEventListener('click', close);
  };
})();
