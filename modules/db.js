var db = require('../articles/db'),
  path = require('path'),
  fs = require('fs'),
  jsdiff = require('diff'),
  memcache = require('./memcache'),
  debug = require('debug')('miniWiki:db');

// get latest revision ID of an article
function getLatestID (name) {
  debug('getLatestID %s', name);
  return db[name];
}

// update revision id of an article
function updateID (name, id, cb) {
  debug('updateID %s %s', name, id);
  db[name] = id;
  fs.writeFile(path.join(__dirname, '../articles/db.json'), JSON.stringify(db),
    function (err) {
      debug('id updated %s %s', name, id);
      if (err) {
        cb('error');
      } else {
        cb();
      }
    }
  );
}

// return path to the article
function getPath (name, id) {
  return path.join(__dirname, '../articles/' + name + '.' + id + '.json');
}

function getArticle (name, id, cb) {
  id = id || getLatestID(name);

  debug('getArticle %s %s', name, id);
  memcache.get(name, function (data) {
    if (data !== 'error') {
      debug('getArticle: Found in memcache %s', name);
      cb(null, data);
    } else {
      // if data is not found in memcache read from the disc
      debug('getArticle: Checking path %s %s', getPath(name, id));
      if (id) {
        fs.readFile(getPath(name, id), 'utf8', function (err, data) {
          // if there is no error on reading the file from disc,
          // cache it in memcache
          if (!err) {
            // this is async, but still we don't need to wait for it -
            // to finish
            memcache.add(name, data);
          }
          // call original cb
          cb(err, data);
        });
      } else {
        debug('getArticle id not found %s', name);
        cb({
          code: 'ENOENT'
        });
        return;
      }
    }
  });
}

// TODO: Finish conflict resolving
function resolveConflictsWithLatest (data, cb) {
   // get original article to create diff
  getArticle(data.name, data.id, function (original) {
    // create patch with the data we got from client
    // var patch = jsdiff.createPatch(data, original);

    // get latest article to apply the patch
    getArticle(name, null, function (latest) {
      //apply the patch and call cb
      //newdata = JSON.parse(jsdiff.apply(latest, patch));
      //newdata.id = ++latest.id;
      //cb(newdata);
    });
  });
}

function writeArticle (data, cb) {
  var latest = getLatestID(data.name),
    json, id;

  debug('writeArticle %s %s, latest: %s', data.name, data.id, latest);
  if (id !== latest) {
    // handle conflicts
    // resolveConflictsWithLatest(data)
  }
  id = latest + 1;

  json = JSON.stringify({
    name: data.name,
    title: data.title,
    id: id,
    timestamp: Date.now(),
    data: data.data
  });


  debug('writing to memcache: %s %s', data.name, id);
  memcache.replace(data.name, json, function (memcache_err, value) {
    if (!memcache_err) {
      cb(memcache_err, value);
      return;
    }

    debug('writing to disc: %s %s', data.name, id);
    // this is async, we need to wait for it to be done, only if
    // memcache is not working.
    fs.writeFile(getPath(data.name, id), json, function (file_err) {
      if (file_err) {
        debug('writing to disc failed %s %s', data.name, id);
      } else {
        debug('wrote %s %s', data.name, id);
      }
      // call cb only if memcache failed
      if (memcache_err) {
        cb(file_err, json);
      }
    });
    updateID(data.name, id, cb);
  });
}

module.exports = {
  getLatestID: getLatestID,
  getArticle: getArticle,
  writeArticle: writeArticle
};
