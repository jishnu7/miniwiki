var fs = require('fs'),
  path = require('path'),
  debug = require('debug')('miniWiki:article'),
  db = require('./db');
  //db = require('../articles/db');

function read (name, id) {
  return new Promise(function (resolve, reject) {
    debug('read %s', name);

    db.getArticle(name, id, function (err, data) {
      if (err) {
        debug('error on reading %s', name);
        reject(err.code);
      } else {
        try {
          resolve(JSON.parse(data));
        } catch (e) {
          debug('json parse failed %s', name);
          reject(e);
        }
      }
    });
  });
}

function write (data) {
  return new Promise(function (resolve, reject) {
    debug('write %s', data.name);
    db.writeArticle(data, function (err, update) {
      if (err) {
        debug('writeArticle %s', err);
        reject();
      } else {
        debug('writeArticle: resolve');
        resolve(update);
      }
    });
  });
}

module.exports = {
  read: read,
  write: write
};
