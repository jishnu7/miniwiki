var Memcached = require('memcached'),
  memcached = new Memcached(
    // TODO: move this to config
    // server and port
    "localhost:11211",
    {
      retries: 0,
    }
  ),
  // lifetime for a data to be kept in memcache
  // in seconds
  lifetime = 3600,
  // prefix for the key
  prefix = 'miniwiki_',
  debug = require('debug')('miniWiki:memcache');

// Catch server faults
// TODO:: Handle this
memcached.on('failure', function (details) {
});

// get data from memcache
function get (key, cb) {
  debug('get %s', key);
  memcached.get(prefix + key, function (err, data) {
    if (err || !data) {
      debug('missing: %s', key);
      cb('error');
    } else {
      debug('found: %s', key);
      cb(data);
    }
  });
}

// add data to memcache
function add (key, value) {
  debug('add: %s', key);
  memcached.add(prefix + key, value, lifetime, function (err) {
    if (err) {
      debug('could not save: %s', key);
    } else {
      debug('saved: %s', key);
    }
  });
}

// add data to memcache
function replace (key, value, cb) {
  debug('replace: %s', key);
  memcached.replace(prefix + key, value, lifetime, function (err) {
    if (err) {
      debug('could not save: %s', key);
      debug(err);
      cb('error');
    } else {
      debug('saved: %s', key);
      cb(null, value);
    }
  });
}

module.exports = {
  get: get,
  add: add,
  replace: replace
};
