// module to return fibanocci number
// 0, 1, 1, 2, 3, 5, 8, 13 etc

function fibanocci(num) {
  if (num <= 0) {
    return 0;
  } else if (num <= 2) {
    return 1;
  } else {
    return fibanocci(num - 1) + fibanocci(num - 2);
  }
}

module.exports = fibanocci;
