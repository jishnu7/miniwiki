# MiniWiki

## Requirements
- memcached
- latest version of chrome/firefox

## How to use
$ npm install
$ npm start

## TODO
- Move article/db.json, which stores revision ids, to a DB.
- Make diff work. Current format, json is making it difficult for diff and patch to work.
  Might have to change to some other format to make it work.
- Interface to handle edit conflicts in client side.
- Error handling on memcache side.
- Move memcache configurations to a config file.
- Show last update time in client side UI.
