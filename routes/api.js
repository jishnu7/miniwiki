var express = require('express'),
  router = express.Router(),
  debug = require('debug')('miniWiki:api'),
  sanitizeHtml = require('sanitize-html'),
  sanitizer = require('sanitize')(),
  article = require('../modules/article');

router.post('/', function(req, res, next) {
  var data, errors, write;

  debug('api %s %s', req.body.name, req.body.id, req.body.title);

  req.checkBody('id', 'Invalid id').notEmpty().isInt();
  req.checkBody('name', 'Invalid name').notEmpty();
  req.sanitize('name').escape().trim();
  req.checkBody('title', 'Invalid name').notEmpty();
  req.sanitize('title').escape().trim();

  data = sanitizeHtml(req.body.data);

  errors = req.validationErrors();
  console.log(errors);
  if (errors) {
    next({
      message: 'There are validation errors.'
    });
  } else {
    write = article.write({
        name: req.body.name,
        id: req.body.id,
        title: req.body.title,
        data: data
      });

    write.then(function (update) {
      res.send(update);
    });

    write.catch(function (err) {
      next({
        message: 'Something went wrong.'
      });
    });
  }
});

module.exports = router;
