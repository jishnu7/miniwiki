var express = require('express');
var router = express.Router();
var article = require('../modules/article');
var fibanocci = require('../modules/fibanocci');

/* GET articl page. */
router.get('/', function(req, res, next) {
  var read = article.read('Latest_plane_crash');

  // to simulate load
  fibanocci(34);

  read.then(function (data) {
    res.render('article', data);
  });

  read.catch(function (err) {
    console.log(err);
    var messages = {
      ENOENT: 'Article not found.'
    };

    next({
      message: messages[err] || 'Something went wrong.'
    });
  });
});

module.exports = router;
